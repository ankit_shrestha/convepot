<?php

class Profile extends Eloquent{
	
	protected $fillable = array('bio','noofposts');
	protected $guarded = array('id','user_id');

	public $timestamps = false;

	public function user(){
		return $this->belongsTo('User');
	}

	public function incrementPost($data){
		$this->noofposts = $this->noofposts + 1;
	}

}