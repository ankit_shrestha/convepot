<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $guarded = array('id','login_id');

	protected $hidden = array('password','remember_token');

	protected $fillable = array('username','bio','noofposts');

//one-to-one relation(one login can have only one user)
	public function login(){
		return $this->belongsTo('Login');
	}

//many to one relation(one user can have many posts)
	public function posts(){
		return $this->hasMany('Post');
	}

	public function profile(){
		return $this->hasOne('Profile');
	}


}
