<?php

class Post extends Eloquent{
	
	protected $guarded = array('id','user_id');
	protected $fillable = array('description','filename','noofforks');

//one-to-many relation(each post belogns to a user)
	public function user(){
		return $this->belongsTo('User');
	}

}