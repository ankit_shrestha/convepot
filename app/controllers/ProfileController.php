<?php

class ProfileController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($username)
	{
		/* $id is the username here 
		/* Reference to the fetch the profile of the user defined
		*/
		$user = DB::table('users')->where('username',$username)->first(); // $id is username here
		$profile = User::find($user->id)->profile;		//toArray is used to get the array of array of 
					 										//array to get the only required data;

		/*Get all the posts done by the $user*/
		/*May be sent as ajax or any other method
		/*No of forks should also be displayed but not now.
		*/
		//$posts = User::find($user->id)->posts->first()->paginate(5)	; //paginate this
		$posts = Post::where('user_id',$user->id)->paginate(5);




		/*Data for the view file
		/*Username and lists
		*/
		$username = $user->username;
		$bio = $profile->bio;
		$noofposts = $profile->noofposts;


		$data = array('username'=>$user->username,'bio'=>$profile->bio,'noofposts'=>$profile->noofposts);
		//$uname = array('username'=>$user->user1name);
		/*In view file data is displayed.
		*/
		return View::make('profile')->with('posts',$posts)
									->with('username',$username)
									->with('bio',$bio)
									->with('noofposts',$noofposts);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($username)
	{
		$user = DB::table('users')->where('username',$username)->first();
		$profile = User::find($user->id)->profile;

		/* This data is then send to get the user's information to view file
		*/
		Session::put('profile_id',$profile->id); // So that the data is available to next page and all.
		return View::make('editProfile')->with('bio',$profile->bio)->with('username',$user->username); 
			
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
	/* DATA SEND VIA THE FORM */
		$data = Input::all();
		
		$user = Auth::user();
		$old_user_name = $user->username; // For renaming the directory.
		
		/* $data['username'] and $data['bio'] available */
		
		/*Fetch the profile */
		$profile = User::find($user->id)->profile;

		/* set the edited values into the user and respective profile (Password is hashed username)*/
		$user->username = $data['username'];
		$user->password = Hash::make($user->username);
		$profile->bio = $data['bio'];

		$user->save();
		$profile->save();

		/*Rename the directory in the public path so that the images can be accessed*/
		$path_to_image = public_path()."/images/";
		try{
			if( rename($path_to_image.$old_user_name, $path_to_image.$data['username'] ) ){
				//return "SUCCESSFULLY DIRECTORY CHANGED";
			}else{
				throw new Exception("UNABLE TO RENAME DIRECTORY");
			}
		}catch(Exception $e){
			return $e;
		}

		/*Redirect to profile*/
		$redirect_url = "profile/".$user->username;
		return Redirect::to($redirect_url);

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function test(){
		return "TEST CALLED";
	}

}
