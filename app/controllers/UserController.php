<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		/*Check the user already exists or not by UserController@is_Registered method*/
	$is_registered = App::make('UserController')->is_Registered(Input::get('username'));	//return boolean

	//If no user found, new instance of login, user , and profile is created, else redirected to signin.
	if( $is_registered){
		Session::flash('error',"UserName taken, try other name");
		return Redirect::to('/signup');
	}else{
	//Required data for the User instance . bio is for the data in Profile instance
		$username = Input::get('username');
		$bio = Input::get('bio');
		$password = Hash::make($username);

	//Creation of the Login instance
		$login = new Login();
		$login->string = Session::get('string');
		$login->save();

	//get the id for the instace of the Login class
		$login_instance = Login::find($login->id);
		

	//User instance to entry into the database;
		$user = new User();
		$user->username = $username;
		$user->password = $password;
		$user->login_id = $login_instance->id;
		$user->save();

	//get the user_id of the User instance
		$uid = $user->id;

	//Profile instance for newly created user
		$profile = new Profile();
		$profile->user_id = $uid;
		$profile->bio = $bio;
		$profile->noofposts = 0;
		$profile->save();

//Redirect to some page. May be welcome page or page with post. Or reload from the begining(with facebook using Session data)


/* Email functionality to be included
*/


		return Redirect::to('/home');
	}


	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function is_Registered($name){
		try{
			if( DB::table('users')->where('username',$name)->first() ){
				throw new Exception("User exists");
			}else{
				return false;	//i.e User doesn't exists.
			}
		}catch(Exception $e){
				return true;	//i.e User exists.
		}
	}

}

