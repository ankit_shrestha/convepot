<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	/* Home view */
	public function index()
	{	

		//Unuse this if you want to use byDate and byFork
		//Session::flush();

		/*
		if( "true" == Session::get('byForks') ){
			/*This is the ajax call to get the post by noofforks order in the home page
				
			$posts = Post::orderBy('noofforks','desc')->paginate(5);
			Session::forget('byForks');
			return $posts;
		
		}else if( "true" == Session::get('byDate') ){
		/* Display the recent posts by dates
			
			$posts = Post::orderBy('created_at','desc')->paginate(5);
			Session::forget('byDate');
			return $posts;
		}else{
		/* This is called for the first time 
		/* Default call, this is sorting the posts by Date
		*/

		$posts = Post::orderBy('created_at','desc')->paginate(5);
		return View::make('home')->with('posts',$posts);

		//}		if's ending
		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	/*The data for the post**/
		$description = Input::get('description');
		//var_dump($description);
		$filename = Input::file('filename');
		//var_dump($filename`
	

	/* Get the user_id from the Authentication
	*/
		$user_id= Auth::id();
		//var_dump($user_id);

	
	/* File validation */




	/*Insert into the database the url of the file*/ 
	/*Store the file into the folder and refer it using the database*/
		$path = public_path('images/'.Auth::user()->username,$filename->getClientOriginalName());
		$filename->move($path,Input::file('filename')->getClientOriginalName());


	/*A new Post instance for the data entry for the post*/
		$newPost = new Post;
		$newPost->user_id = $user_id;
		$newPost->description = $description;
		$newPost->filename = Input::file('filename')->getClientOriginalName();
		$newPost->noofforks = 0;
		$newPost->save();

		

	/*Add 1 to the noofposts to the user profile table*/
		$profile = User::find(Auth::user()->id)->profile; //Test this
		$profile->incrementPost(1);
		$profile->save();
		//return "Successfully Posted";
	
	/*Redirect to home*/
		return Redirect::to('/home');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Post::find($id);

		/*EDIT THIS POST*/
		return $post; //do something for this
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,$command)
	{
		/*id is the post_id here*/
		
		if( $command == "insert"){
			/* noofforks is increased */
		$post = Post::find($id);
		$post->noofforks = $post->noofforks + 1;
		$post->save();
		return $post->noofforks;
	
		}else{  //command is to decrease the fork 

		$post = Post::find($id);
		$post->noofforks = $post->noofforks - 1;
		$post->save();
			return $post->noofforks;
		}

	}


	/**
	 * 
	 *
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		/*Determine whose post it is
		/*Get the username so that page can be reloaded
		*/
		$post_of = Post::where('id',$id)->first();		//get the id of the post.
		$user = User::where('id',$post_of->user_id)->first();//get the user of the same post.


		/* Also delete it from the posts_users table to unlink the post */
		$post_in_posts_users_table = DB::table('posts_users')->where('post_id',$id)->delete();

		/* Fetch post to delete*/
		$post = Post::where('id',$id)->delete();

		/* Reduce the noofposts from the profile table by 1*/
		$profile = User::find($user->id)->profile;
		$profile->noofposts = $profile->noofposts - 1;
		$profile->save();
		return "Successfully Deleted";

	}


}
