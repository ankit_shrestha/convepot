<?php

class LoginController extends BaseController{

/* Login with string component obtained from the facebook*/
public function check($data){
		$string['data'] = $data;

		$login_id = new Login;
		$login_id = DB::table('logins')->where('string',$string['data'])->first();

		$user = new User;
		$user = DB::table('users')->where('login_id',$login_id->id)->first();

		$credentials = array('username'=>$user->username,'password'=>$user->username);
		if( Auth::attempt($credentials)){
			//echo "Authenticated";
			return Redirect::to('/home');
		}else{
			return Redirect::to('/login');
		}

}

/* Login with facebook */
public function checkFB(){
	$code = Input::get('code');
    if (strlen($code) == 0) return Redirect::to('/')->with('message', 'There was an error communicating with Facebook');

    $facebook = new Facebook(Config::get('facebook'));
    $uid = $facebook->getUser();
    
    if ($uid == 0) return Redirect::to('/')->with('message', 'There was an error');

    $me = $facebook->api('/me');

    /*me has name and id */ 
    /*concat it to make a login string*/
 	$login_string = $me['name'].$me['id'];
    
    Session::put('string',$login_string);

    /*If user already exists make attempt to check login else make a new profile of signup*/
    if( DB::table('logins')->where('string',$login_string)->first()){
   		return	App::make('LoginController')->check($login_string);
    }else{
    	return Redirect::to('/signup');
    }	
}	
	

}