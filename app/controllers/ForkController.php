<?php 

class ForkController extends \BaseController{
	
	/*Called when ever the post is forked updated in table posts_users which is pivot table
	*/
	public function entry(){
		
		/*Data from the ajax request.
		/*Data for update to increase fork counds
		*/
		$data = Input::all();
	
		$post_id = $data['post_id'];
		$user_id = $data['user_id']; //The id who forked the post.
		

		/*checking if already forked, if it is ,it is unforked 
		/*Else it is forked
		*/
		if( DB::table('posts_users')->where('post_id',$post_id)->where('user_id',$user_id)->first()){
				/*true already forked. Now unfork this*/
				return $this->deletion($post_id,$user_id);
		}else{
				/* this is forking processing increasing fork no in post and update posts_users table*/
			if(DB::table('posts_users')->insert(array(
									'post_id'=>$post_id,
									'user_id'=>$user_id)) ){
				
			/*Increase the no of forks in the posts table for this specific post*/
				return App::make('PostController')->update($post_id,"insert"); //call the controller


				//return "FORKED".$post_id;		
			}
		}


	}


	/*Called when ever the post is unforked updated in table posts_users which is pivot table
	*/
	public function deletion($p,$u){
		/*delete from the posts_users table*/
		DB::table('posts_users')->where('post_id',$p)->where('user_id',$u)->delete();	
		
		/* Also decrease the no of count of the fork */
		return App::make('PostController')->update($p,"delete");
	}
}