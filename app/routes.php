<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::resource('/home','PostController');

Route::get('/',function(){
	return Redirect::to('/home');
});

/*ASSUME A STRING FROM FACEBOOK API/ SOMETHING AS INITIAL INPUT 
/*THIS CREATES A FIRST USER FOR AUTHENTICATION
*/
Route::get('/login/{data}',['uses'=>'LoginController@check']);
//Get the data as a string
//String assumed here
//Take more than string, like email password etc.

/*Tell user to login via facebook as default login uses a string from facebook*/
Route::get('/login',function(){
	return View::make('login');
});

/*FOR SIGNUPS INTO THE APP*/
Route::get('/signup',function()
{
	//Login string is set in Session already.	( LoginController@checkFB)
	return View::make('signup');
});
Route::post('signup',array('uses'=>'UserController@create'));



/*For a new post to entry
/*Only Authenticated user are allowed*/
Route::get('newpost',array('before'=>'auth',function(){
	return View::make('newpost');
}));
Route::post('newpost',array('uses'=>'PostController@create'));


/*Visit the profile of anyone*/
/* User must be authenticated to view someone's profile */
Route::group(array('before'=>'auth'),function(){
	/*Display the profile of any other user*/
	Route::get('profile/{username}',array('as'=>'profile','uses'=>'ProfileController@show')); //Get the profile

	/*Register a fork in any post of another user*/
	Route::get('forkEntry',['uses'=>'ForkController@entry']);


	/*Edit a post. profile.blade.php calls this route. */
	Route::resource('editPost/{post}/edit','PostController@edit');

	/*Delete( destroy ) a post*/
	Route::any('deletePost/{post_id}','PostController@destroy');

	/* Edit your profile */
	Route::any('editProfile/{username}/edit','ProfileController@edit');

	/* Update the profile*/
	Route::post('updateProfile','ProfileController@update');

});


/* Send username to authenticated or not authenticated profile for view */
Route::get('getUsername',function(){
	$data = Input::all();
	$post = DB::table('posts')->where('id',$data['post_id'])->first();
	$user = DB::table('users')->where('id',$post->user_id)->first();
	return $user->username;
});




/*TEST*/
Route::get('/test',function(){
		return public_path();
});






/*FOOTER pages*/
Route::get('/aboutUs',function(){
	return View::make('aboutUs');
});
Route::get('/privacyPolicy',function(){
	return View::make('privacyPolicy');
});
Route::get('/terms',function(){
	return View::make('terms');
});


/*Logout of the app*/
Route::get('/logout',function(){
	Auth::logout();
	return View::make('logout');
});




/*Check with facebook*/
Route::get('checkFB',function(){
	$facebook = new Facebook(Config::get('facebook'));
    $params = array(
        'redirect_uri' => url('/checkFB_Code'),
        'scope' => 'email',
    );
 	  
    	return Redirect::to($facebook->getLoginUrl($params));
});
Route::get('/checkFB_Code','LoginController@checkFB');