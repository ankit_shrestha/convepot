<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePostsUsersPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts_users',function($tb){
			$tb->increments('id');
			$tb->integer('post_id')->unsigned();
			$tb->foreign('post_id')->references('id')->on('posts');
			$tb->integer('user_id')->unsigned();
			$tb->foreign('user_id')->references('id')->on('users');
			$tb->timestamps();
		});
		//
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts_users');
		//
	}

}
