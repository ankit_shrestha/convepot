<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPostsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('posts_users',function($tb){
			$tb->integer('post_id');
			$tb->integer('user_id');
		});
		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('posts_users',function($tb){
			$tb->dropColumn('posts_id');
			$tb->dropColumn('user_id');
		});
		//
	}

}
