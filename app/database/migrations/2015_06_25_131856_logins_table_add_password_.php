<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoginsTableAddPassword extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('logins',function($tb)
		{
			$tb->string('password');
			$tb->rememberToken();
		});
		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('logins',function($tb)
		{
			$tb->dropColumn('password');
		});
		//
	}

}
