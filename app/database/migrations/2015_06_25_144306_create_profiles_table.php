<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profiles',function($tb){
			$tb->increments('id');
			$tb->integer('user_id')->unsigned();
			$tb->foreign('user_id')->references('id')->on('users');
			$tb->text('bio');
			$tb->integer('noofposts');
		});
		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profiles');
		//
	}

}
