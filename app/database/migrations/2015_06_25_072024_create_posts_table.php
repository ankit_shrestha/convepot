<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//posts table
		Schema::create('posts',function($tb){
			$tb->increments('id');
			$tb->integer('user_id')->unsigned();
			$tb->foreign('user_id')->references('id')->on('users');
			$tb->text('description');
			$tb->string('filename'); // for the image (filename)
			$tb->integer('noofforks');

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
			//
	}

}
