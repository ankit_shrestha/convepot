<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//users table
		Schema::create('users',function($table){
				$table->increments('id');
				$table->integer('login_id')->unsigned();
				$table->foreign('login_id')->references('id')->on('logins');
				$table->string('username')->unique();
				$table->text('bio');
				$table->integer('noofposts');
				$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::drop('users');
		//
	}

}
