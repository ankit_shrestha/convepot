<!-- For the template of the header part of the site 
	It is same in the site

	If user is authenticated add two button of going to profile and add new post from any where
-->
<div>
	<div align="center">
		<button id="homeButton">CONVEPOT</button>
	<!-- Display the profile button only if the user is logged in, if not provide a login button
		Button to go to the profile of his directly,newPOst and also logout.
	-->	
		<?php 
			if(Auth::check()){ 
				$username = Auth::user()->username;
				
				/* Display button for going to your own profile*/
				echo "<button id='profileButton'>";echo "$username";echo "</button>";

				/* Display button for new post*/
				echo "<button id='newPostButton'>";echo "NEW POST";echo "</button>";

				/*Display the button for the log out purpose*/
				echo "<button id='logOutButton'>";echo "LOGOUT"; echo "</button>";
			}else{
				$username = "";		//if no user, set username = empty string so that JS can refrence it.
				echo "<button id=logIn>";echo "LOGIN";echo "</button>";
			}
		?>
	</div>
	<script>
		
		var homeUrl = "{{ url('/home') }}";	//To go to the home directly.
		var profileUrl = "{{ url('/profile')}}";	//visit your own profile
		var	newPostUrl = "{{ url('/newpost')}}";	//for the new post 
		var logInUrl = "{{ url('/login')}}";		//to go to the login page
		var logOutUrl = "{{url('/logout')}}";  		//Url for the logout purpose.

		var username = "<?php echo $username;?>"; // Username of the authenticated user, set above if user is logged in


		$(document).ready(function(){
			$(document).on('click',"#homeButton",function(){
				window.location = homeUrl;
			});

			$(document).on('click',"#profileButton",function(){
				window.location = profileUrl + "/" + username;
			})
		});

		$(document).ready(function(){
			$(document).on('click',"#newPostButton",function(){
				window.location = newPostUrl;
			});
		});

		$(document).ready(function(){
			$(document).on('click',"#logOutButton",function(){
				window.location = logOutUrl;
			});
		});

		$(document).ready(function(){
			$(document).on('click',"#logIn",function(){
				window.location = logInUrl;
			});
		});

	</script>
</div>