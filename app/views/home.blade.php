
<!-- This page is the home page.
	Displays all the posts of the users with account.
	This page can be viewed by the users even with out the user account.
	Restricts the user to fork. 
	To fork the post, the user must sign in first.(if there is no account then sign up)
	-->

@extends('layouts.default')

@section('content')
	<body>
		<!--<button id='byForks'>SEARCH BY FORKS</button> <button id='byDate'>SEARCH BY DATE</button>
		-->
		@foreach($posts as $p)
		<div align="center">	
  			<?php 	$user = User::where('id',$p->user_id)->first(); ?>
			<p id="image">{{HTML::image("images/{$user->username}/{$p->filename}",'Picture',array('height'=>400,'width'=>600)) }}</p>
			<p id="description">{{$p->description }}</p>  <!-- Comment this -->
			<p id="noofforks">{{$p->noofforks}}</p>
			<p id="post_id">{{$p->id}}</p>
		</div>
		@endforeach
		<div align="center">
			<?php 
			//This is for the pagination.
			echo $posts->links();
			?>
		</div>

<script>
var userName ="";   
//User of the post, not the Auth::user(). Then goes to the profile of that user if authenticated*/

var baseUrl = "{{ url('profile/')}}";
var newUrl = "";

	/* On click to the image make user to go to the profile page */
	$(document).ready(function(){
		$(document).on("click","#image",function(){
				var post_id =  $(this).parent().find("#post_id").html() ;
				$.ajax({
					url: '../getUsername',
					data : {
							post_id : post_id
						},
					success : function(data){
						 window.userName = data;	
							 /* Go to the profile of that user. make a url. / profile/{username}*/
 						
					/* Redirect a rout to go to the profile of that user obtained from abova ajax call*/
						window.location = baseUrl + '/' + userName;

 						 }
				});

		
				

		});
	});
	$(document).ready(function(){
		$(document).on('click','#byForks',function(){
			<?php Session::put('byForks',"true"); ?> //Set to get by the no of the forks
			$.ajax({
				url: '../home',
				type: 'GET',
				success: function(data){
					//Data response from the ajax call
					var posts = data['data'];
					var post_count = 0 ; // count for the posts
					//Get individual count of the divison
					var divison_count = 0;
			

			/*
					//Get all the divisions.
					divison = document.getElementsByTagName('div');

					for(var div_no = 0 ; div_no < divison.length ; div_no++){
//						divison[div_no].getElementById('description').innerHTML = "TEST";
						//$("divison[div_no]").find("#description").html("TEST");
						//console.log(divison[div_no].getElementById('description').innerHTML);
						console.log( $("divison[div_no]").children("#description").html());
					}
			*/

					$(document).ready(function(){
						$("div").each(function(){

							var new_des = posts[post_count].description;
							var new_noofforks = posts[post_count].noofforks;
							var new_image = posts[post_count].filename;
							var username; // for image display username

							$(this).find("#description").html(new_des);
							$(this).find("#noofforks").html( new_noofforks);
							
							//Get the username for the image and attach for the filename to display the image



							//	$(this).find("#description").html()
							

							post_count++;
						});
					});

				}	//success ends
			});

		});
	});


		$(document).on('click','#byDate',function(){
			<?php Session::put('byDate',"true"); ?> //Set to get by the Date
			$.ajax({
				url: '../home',
				type: 'GET',
				success: function(data){
					//Data response from the ajax call
					var posts = data['data'];
					var post_count = 0 ; // count for the posts
					//Get individual count of the divison
					var divison_count = 0;

					$(document).ready(function(){
						$("div").each(function(){

							var new_des = posts[post_count].description;
							var new_noofforks = posts[post_count].noofforks;
							var new_image = posts[post_count].filename;

					
							$(this).find("#description").html(new_des);
							$(this).find("#noofforks").html( new_noofforks);
							
							//Get the username for the image and attach for the filename to display the image




							//	$(this).find("#description").html()
							

							post_count++;
						});
					});			
				}
			});
		});		

		//The section of the home layout to go the home
		$(document).ready(function(){
			$(document).on('click',"#homeButton",function(){
				window.location = homeUrl;
			});
		});




</script>

</body>
@stop
