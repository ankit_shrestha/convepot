<!-- This will be the default page.
	Every page will contain this lagyout
	-->

<html>

<head>
	@include('includes.head')
</head>
	
<body>
	<div>
		<header>
			@include('includes.header')
		</header>

		<div>
			<!-- Yield the contents here -->
			@yield('content')
		</div>

		<footer>
			@include('includes.footer')
		</footer>
	</div>
</body>

</html>