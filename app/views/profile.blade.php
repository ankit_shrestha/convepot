<!-- Each and every post will have a id of its own ie post_id, 
	the user logged in will click to fork it. 
	The user whose post is forked will get a count increment of 1 after it is forked by the another user.
	
	The every post after clicked will send a ajax request to increase the count of the forks in that post
-->


<!-- Display the no of forks as well -->

@extends('layouts.default')

@section('content')
<HTML>

		<body>
		
		<div align="center">
			<span id='username'>{{$username}}</span>
			<span id='bio'>Bio : {{$bio}}</span>
			<span id='noofposts'>Posts: {{ $noofposts}}</span>
		</div>

		<div align="center">
		<?php
			/*Username is send to check if the porfile is of the Auth::user() type.
			*/
			$is_user =  User::where('username',$username)->first();
			if( (Auth::user()->id == $is_user->id) ){	//if the user is looking at his own profile let him edit.
				echo "<button id='editProfile'>EDIT YOUR PROFILE</button>";
			}
		?>
		</div>
		
			@foreach( $posts as $p)
			<div align="center">
				<p id="description">{{$p->description }}</p>
				<p id="user_id">{{$p->user_id }}</p>
				<p id="post_id">{{$p->id }}</p>
				<p id="noOfForks">{{ $p->noofforks }}</p>
				<p id="image">{{HTML::image("images/{$username}/{$p->filename}",'Picture',array('height'=>400,'width'=>600)) }}</p>
				
				<?php
					$user = User::where('id',$p->user_id)->first();
					if( (Auth::user()->id == $user->id)){	//if the user is looking at his own profile let him edit.
						echo "<button id='editButton'>Edit</button>";
						echo "<button id='deleteButton'>Delete</button>";
						}
				?>
			</div>
			@endforeach
			
			<div align="center">
			<?php 
			//This is for the pagination.
				echo $posts->links();
			?>
			</div>


		

<script>

var homeUrl = "{{ url('/home')}}";	// TO go to the home directly.

var user_id = {{Auth::user()->id}};

/*all are example here, get actual data from the javascript*/
var post_id;  // The post which is forked.
var profile_id; // this is the user whose profile it is.Further use.
var username; //username form the id.
var profile_info;  //used to edit the profile later.

/*assign div id and p id for each element*/
var div_id = 0;
$("div").each(function(div_id){
		$(this).attr('id','id'+div_id);	//assign id dynamically
		div_id = div_id + 1;
});




/*Onclick to any picture a ajax requerst will be sent like
/*Get the id of the photo dynamically. Here assumed obtained
*/
$(document).ready(function(){
	$(document).on('dblclick',"#image",function(){
			
			/*GET THE post no from each div*/
			post_id = $(this).parent().find("#post_id").text(); 
			
			$.ajax({
				//url:'../test',	 //pass it in  a controller.( profile controller and also post controller)
				url: '../forkEntry',
				type: 'GET',
				data: {
					user_id: user_id,	//Who is authenticated
					post_id: post_id,	//post id(get if from javascript)
					},
				success: function(data){
				//redirect to profile with the updated data
				window.location = window.location;
				}
			});
	});
});

$(document).ready(function(){
	$(document).on('click',"#editButton",function(){
		/*Get post id for the editing purpose.
		/*Make sure the user is authenticated in route
		*/		
		post_id = $(this).parent().find("#post_id").text();
		$.ajax({
			url: '../editPost/'+ post_id +'/edit',
			type: 'GET',
			data: {post_id:post_id},
			success: function(data){
				console.log(data);
			}
		});
		
	});
});


$(document).ready(function(){
	$(document).on('click',"#deleteButton",function(){
		/*Get post id for the editing purpose.
		/*Make sure the user is authenticated in route
		*/		
		post_id = $(this).parent().find("#post_id").text();

		if( confirm("Your post will be deleted.Are you sure?")){
		$.ajax({
			url: '../deletePost/'+ post_id,
			type: 'DELETE',
			data: {post_id:post_id},
			success: function(data){
				location.reload();
			}
		});
		}
		
	});
});

$(document).ready(function(){
	$(document).on('click',"#editProfile",function(){
		username = $("#username").text();  // <p>'s data innerHTML

		/*got to editProfile url to register a route*/
		window.location = "/editProfile/"+ username + "/edit";
	
	});
});

	//The section of the home layout to go the home
		$(document).ready(function(){
			$(document).on('click',"#homeButton",function(){
				window.location = homeUrl;
			});
		});

</script>


</body>

</HTML>
@stop